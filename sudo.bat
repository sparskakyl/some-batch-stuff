@ECHO OFF
setlocal DisableDelayedExpansion
SET "batchPath=%~0"
setlocal EnableDelayedExpansion
NET session >nul 2>&1
IF NOT %errorlevel%==0 GOTO GetAdminPrivs
%*
GOTO :EOF

:GetAdminPrivs
SET "vbsGetPrivileges=%temp%\adminp.vbs"
ECHO Set UAC = CreateObject^("Shell.Application"^) > "%vbsGetPrivileges%"
ECHO For Each strArg in WScript.Arguments >> "%vbsGetPrivileges%"
ECHO args = args ^& strArg ^& " "  >> "%vbsGetPrivileges%"
ECHO Next >> "%vbsGetPrivileges%"
IF '%cmdInvoke%'=='1' goto InvokeCmd 
ECHO UAC.ShellExecute "!batchPath!", args, "", "runas", 1 >> "%vbsGetPrivileges%"
"%SystemRoot%\System32\WScript.exe" "%vbsGetPrivileges%" %*
DEL "%vbsGetPrivileges%"

:EOF
@ECHO ON